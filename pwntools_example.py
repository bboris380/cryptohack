
from pwn import * # pip install pwntools
import json, base64, codecs
from Crypto.Util.number import long_to_bytes

r = remote('socket.cryptohack.org', 13377, level = 'debug')

def json_recv():
    line = r.recvline()
    return json.loads(line.decode())

def json_send(hsh):
    request = json.dumps(hsh).encode()
    r.sendline(request)


def decode(type,encoded):
   print("\n",type,encoded)
   if type == "base64":
    base64_message = encoded
    base64_bytes = base64_message.encode('ascii')
    message_bytes = base64.b64decode(base64_bytes)
    decoded = message_bytes.decode('ascii')
   elif type == "hex":
    hex_string = encoded
    bytes_object = bytes.fromhex(hex_string)
    decoded = bytes_object.decode("ASCII")
   elif type == "rot13":
   	rot13 = lambda s : codecs.getencoder("rot-13")(s)[0]
   	decoded = rot13(encoded)
   elif type == "bigint":
    hex_string = encoded[2:]
    bytes_object = bytes.fromhex(hex_string)
    decoded = bytes_object.decode("ASCII")
   elif type == "utf-8":
   	decoded = ""
   	for x in encoded:
   		decoded += chr(x)
   print("decoded:",decoded)
   return {"decoded": decoded}


for x in range(0,10000):
 received = json_recv()
 to_send = decode(received["type"],received["encoded"])
 json_send(to_send)#
